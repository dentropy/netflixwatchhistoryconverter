import json
import random
from modules.netflix_csv_parse import convert_csv_to_json
from modules.get_tmdb_id import get_tmdb_id
from modules.use_search_engine import search_to_tmdb

def save_json(netflix_data):
    with open("NetflixViewingHistory.json", 'w') as outfile:
        json.dump(netflix_data, outfile)

netflix_data = convert_csv_to_json("NetflixViewingHistory.csv")

for entry in netflix_data["grouped_entries"]:
    print(entry)
    search_term = "TMDB_ID"
    search_term = random.choice(list(netflix_data["grouped_entries"][entry].keys()))
    search_result = search_to_tmdb(entry + " " + search_term)
    if search_result == None:
        netflix_data["grouped_entries"][entry]["TMDB_ID"] = "ERROR"
    else:
        tmdb_id = search_result["links"][0].split("/")[3] + "/" + search_result["links"][0].split("/")[4].split("-")[0]
        netflix_data["grouped_entries"][entry]["TMDB_ID"] = tmdb_id
        print(tmdb_id)
        print("\n\n")

for entry in netflix_data["single_entries"]:
    print(entry)
    search_result = search_to_tmdb(entry)
    if search_result == None:
        netflix_data["single_entries"][entry]["TMDB_ID"] = "ERROR"
    else:
        tmdb_id = search_result["links"][0].split("/")[3] + "/" + search_result["links"][0].split("/")[4].split("-")[0]
        netflix_data["single_entries"][entry]["TMDB_ID"] = tmdb_id
        print(tmdb_id)
        print("\n\n")

save_json(netflix_data)