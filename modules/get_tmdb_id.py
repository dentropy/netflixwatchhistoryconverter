import tmdbsimple as tmdb
import json
with open('tmdb_api_key.json') as json_file:
    tmdb.API_KEY = json.load(json_file)["tmdb_api_key"]

def get_tmdb_id(search_term, media_type):
    if media_type == "movie":
        movie_id = get_movie_id(search_term)
        if movie_id != False:
            return(movie_id)
        else:
            tv_id = get_tv_id(search_term)
            if tv_id == False:
                return("Error")
            else:
                return(tv_id)
    elif media_type == "tv":
        tv_id = get_tv_id(search_term)
        if tv_id == False:
            movie_id = get_movie_id(search_term)
            if movie_id != False:
                return(movie_id)
            else:
                return("Error")
        else:
            return(tv_id)

def get_movie_id(search_term):
    search = tmdb.Search()
    response = search.movie(query=search_term)
    print("Movie", str(len(search.results)))
    if len(search.results) == 0:
        return False
    else:
        return(search.results)

def get_tv_id(search_term):
    search = tmdb.Search()
    response = search.tv(query=search_term)
    print("TV", str(len(search.results)))
    if len(search.results) == 0:
        return False
    else:
        return(search.results)