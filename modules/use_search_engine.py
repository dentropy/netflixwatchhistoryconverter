from search_engine_parser.core.engines.google import Search as GoogleSearch
from search_engine_parser.core.engines.duckduckgo import Search as DuckduckgoSearch
from search_engine_parser.core.engines.bing import Search as BingSearch
from search_engine_parser.core.engines.yahoo import Search as YahooSearch
import random
import time

def search_to_tmdb(search_term, use_serach_engine=1):
    search_term += ' site:https://www.themoviedb.org/'
    search_args = (search_term, 1)
    for use_serach_engine in range(4):
        result = get_result(search_args, use_serach_engine + 1)
        if result != None:
            break
    print(result)
    return(result)

def get_result(search_args, engine_choice=None):
    answer = None
    if engine_choice == None:
        engine_choice = random.randint(1,4)
    if engine_choice == 1:
        search_engine = GoogleSearch()
    elif engine_choice == 2:
        search_engine = DuckduckgoSearch()
    elif engine_choice == 3:
        search_engine = BingSearch()
    elif engine_choice == 4:
        search_engine = YahooSearch()
    search_engine.clear_cache()
    try:
        answer = search_engine.search(*search_args)
    except Exception as e:
        print(e)
    return(answer)