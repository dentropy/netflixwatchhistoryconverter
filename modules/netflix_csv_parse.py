#!/usr/bin/python3

import csv
import json

def get_netflix_data(csv_file_name):
    netflix_data = {
        "alphabetical_ordered_titles":[]
    }
    with open(csv_file_name, 'r') as netflix_csv_file:
        csv_reader = csv.reader(netflix_csv_file)
        header = next(csv_reader)
        if header != None:
            for row in csv_reader:
                netflix_data["alphabetical_ordered_titles"].append([row[0], row[1]])
        netflix_data["alphabetical_ordered_titles"] = sorted(netflix_data["alphabetical_ordered_titles"], key=lambda x: x[0])
    return(netflix_data)

def watched_more_than_once(titles):
    shorter_titles = []
    for i in titles:
        shorter_titles.append(i[0])
    just_titles = []
    for i in range(len(shorter_titles)-1):
        if shorter_titles[i] not in just_titles and shorter_titles[i] == shorter_titles[i+1]:
            just_titles.append(shorter_titles[i])
    return(just_titles)

def get_shows(titles, watched_more_than_once):
    just_shows = {}
    for title in titles:
        if title[0] in watched_more_than_once:
            if title[0] not in just_shows:
                just_shows[title[0]] = {"episodes": [ title[1:] ] }
            else:
                just_shows[title[0]]["episodes"].append(title[1:])
    return(just_shows)

def get_single_entrys(titles, watched_more_than_once):
    single_entries = {}
    for title in titles:
        if title[0] not in watched_more_than_once:
            if title[0] not in single_entries:
                single_entries[title[0]] = {"watched": 1}
            else:
                single_entries[title[0]]["watched"] += 1
    return(single_entries)


def convert_csv_to_json(input_file, output_file=None):
    netflix_data = get_netflix_data(input_file)
    sorted_titles_formatted = []
    for title in netflix_data["alphabetical_ordered_titles"]:
        sorted_titles_formatted.append(title[0].split(":"))
    multi_entry = watched_more_than_once(sorted_titles_formatted)
    netflix_data["grouped_entries"] = get_shows(sorted_titles_formatted, multi_entry)
    netflix_data["single_entries"] = get_single_entrys(sorted_titles_formatted, multi_entry)
    if output_file != None:
        with open(output_file, 'w') as outfile:
            json.dump(netflix_data, outfile)
    return(netflix_data)
